//
//  CourseTableViewController.m
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "SubjectTableViewController.h"
#import "AFNetworking.h"
#import "CoursesTableViewController.h"

#define kPurdueIOAPISubjectEndpoint "https://api.purdue.io/odata/Subjects"

@interface SubjectTableViewController ()

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *subjectsArray;
@property (strong, nonatomic) NSArray *filteredSubjectsArray;

// Properties for state restoration.
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation SubjectTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Initialise a SearchResultsTableViewController for Search functionality.
    self.searchResultsTableViewController = [[SearchResultsTableViewController alloc] init];
    
    // Set searchParentCode to enable SearchResultsTableViewController to recognise which its parent UITableViewController.
    self.searchResultsTableViewController.searchParentCode = 1;
    self.searchResultsTableViewController.tableView.delegate = self;
    
    // Initialise UISearchController.
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.barStyle = UIBarStyleBlack;
    [self.searchController.searchBar sizeToFit];
    
    // Add UISearchBar to UITableView.
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.definesPresentationContext = YES;
    
    // Use PurdueIOClient singleton class to fetch data from server.
    PurdueIOClient *client = [PurdueIOClient sharedPurdueIOClient];
    client.delegate = self;
    [client fetchSubjects];
    
    // Start animating UIActivityIndicatorView to denote networking activity.
    [self.networkActivityIndicatorView startAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // For state preservation.
    if(self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        self.searchControllerWasActive = NO;
        
        if(self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            self.searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

#pragma mark - PurdueIOClientDelegate

- (void)purdueIOClient:(PurdueIOClient *)client didFetchSubjects:(id)subjects {
    // Sort subjects according to abbreviation.
    NSArray *unsortedSubjectsArray = [subjects objectForKey:@"value"];
    NSArray *descriptor = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Abbreviation" ascending:YES]];
    self.subjectsArray = [unsortedSubjectsArray sortedArrayUsingDescriptors:descriptor];
    
    // Reload UITableView with sorted data.
    [self.tableView reloadData];
    
    // To remove the UIActivityIndicatorView after data has been fetched.
    [[self navigationItem] setRightBarButtonItem:nil];
}

- (void)purdueIOClient:(PurdueIOClient *)client didFailWithError:(NSError *)error {
    // Initialise an UIAlertController to display an error message to the user.
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Fetching Data"
                                                                             message:@"Unable to get subject data from database."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              
                                                          }];
    [alertController addAction:defaultAction];
    // Present UIAlertController with error message.
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.subjectsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubjectCell" forIndexPath:indexPath];
    
    // Store data of the subject at the given index of subjectsArray.
    NSDictionary *currentSubject = [self.subjectsArray objectAtIndex:indexPath.row];
    
    // Display data of the subject in UITableViewCell.
    cell.textLabel.text = [currentSubject objectForKey:@"Abbreviation"];
    cell.detailTextLabel.text = [currentSubject objectForKey:@"Name"];
    self.name = [currentSubject objectForKey:@"Name"];
    
    // Set colors for UITableViewCell
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor purdueYellowColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // To dismiss the keyboard once Search is done.
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchBarUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // Get the text from UISearchBar.
    NSString *searchText = searchController.searchBar.text;
    
    // Create a mutable copy of subjectsArray so that it can be filtered using the search term entered by the user.
    NSMutableArray *searchResults = [self.subjectsArray mutableCopy];
    
    // Create tokens from search terms.
    NSString *strippedSearchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *searchItems = nil;
    if (strippedSearchText.length > 0) {
        searchItems = [strippedSearchText componentsSeparatedByString:@" "];
    }
    
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        
        // Full subject name matching.
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"Name"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // Subject abbreviation matching.
        lhs = [NSExpression expressionForKeyPath:@"Abbreviation"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        
        NSCompoundPredicate *orMatchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicate];
    }
    
    NSCompoundPredicate *finalCompoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];
    
    SearchResultsTableViewController *tableController = (SearchResultsTableViewController *)self.searchController.searchResultsController;
    tableController.searchResults = searchResults;
    [tableController.tableView reloadData];
}

#pragma mark - UISearchControllerDelegate

- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    // do something after the search controller is presented
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    // do something before the search controller is dismissed
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    // do something after the search controller is dismissed
}

#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const SearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const SearchBarTextKey = @"SearchBarTextKey";
NSString *const SearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:SearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:SearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:SearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:SearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:SearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:SearchBarTextKey];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    CoursesTableViewController *ctrlr = (CoursesTableViewController *)segue.destinationViewController;
    NSDictionary *temp = [self.subjectsArray objectAtIndex:indexPath.row];
    ctrlr.selectedCourse = [temp objectForKey:@"Abbreviation"];
}

@end