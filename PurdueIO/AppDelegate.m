//
//  AppDelegate.m
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "AppDelegate.h"
#import "UIColor+Purdue.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSDictionary *navigationBarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      [UIColor purdueYellowColor], NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navigationBarTitleTextAttributes];
    [[UINavigationBar appearance] setTintColor:[UIColor purdueYellowColor]];
    [[UITableView appearance] setSeparatorColor:[UIColor purdueYellowColor]];
    [[UITableView appearance] setBackgroundColor:[UIColor blackColor]];
    [[UISearchBar appearance] setTintColor:[UIColor purdueYellowColor]];
    
    UIView *colorView = [[UIView alloc] init];
    colorView.backgroundColor = [UIColor grayColor];
    [UITableViewCell appearance].selectedBackgroundView = colorView;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
