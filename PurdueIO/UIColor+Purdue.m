//
//  UIColor+Purdue.m
//  PurdueIO
//
//  Created by Sanjeet Suhag on 26/07/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "UIColor+Purdue.h"

@implementation UIColor (Purdue)

+ (UIColor *)purdueYellowColor {
    return [UIColor colorWithRed:0.89 green:0.68 blue:0.14 alpha:1];
}

@end
