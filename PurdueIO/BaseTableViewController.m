//
//  BaseTableViewController.m
//  PurdueIO
//
//  Created by Sanjeet Suhag on 10/08/15.
//  Copyright © 2015 vnev. All rights reserved.
//

#import "BaseTableViewController.h"

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Give black background to UITableView.
    UIView *blackBackgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    blackBackgroundView.backgroundColor = [UIColor blackColor];
    self.tableView.backgroundView = blackBackgroundView;
    
    // Add networkActivityIndicatorView to the UIRightBarButtonItem.
    self.networkActivityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.networkActivityIndicatorView];
    [[self navigationItem] setRightBarButtonItem:rightBarButtonItem];
}

@end
