# PurdueIO

This is a WIP iOS application for Purdue.io. It utilizes https://purdue.io's API to gather schedule and class data.


## Dependencies

- Cocoapods - can be installed by running `sudo gem install cocoapods`
- AFNetworking - fetched via Cocoapods

## How To Install

1. Open Terminal.app and clone the repo : `git clone https://github.com/vnev/PurdueIO.git`
2. Change directory to the project folder    : `cd PurdueIO/`
3. Run CocoaPods to install the dependencies : `pod install`
4. Open the workspace in Xcode               : `open PurdueIO.xcworkspace`

Here's a GIF of it the app working (**outdated**):

![gif](http://i.imgur.com/rjHNI4P.gif)
