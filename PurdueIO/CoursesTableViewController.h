//
//  CoursesTableViewController.h
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PurdueIOClient.h"
#import "UIColor+Purdue.h"

#import "BaseTableViewController.h"
#import "SearchResultsTableViewController.h"

@interface CoursesTableViewController : BaseTableViewController <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating,PurdueIOClientDelegate>

@property (strong, nonatomic) NSString *selectedCourse;

@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) UISearchController *searchController;

@property (nonatomic, strong) SearchResultsTableViewController *searchResultsTableViewController;

@end
