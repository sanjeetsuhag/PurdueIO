//
//  PurdueIOClient.h
//  PurdueIO
//
//  Created by Sanjeet Suhag on 25/07/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol PurdueIOClientDelegate;

@interface PurdueIOClient : AFHTTPSessionManager

@property (nonatomic, weak) id<PurdueIOClientDelegate>delegate;

+ (PurdueIOClient *)sharedPurdueIOClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)fetchTerms;
- (void)fetchSubjects;
- (void)fetchCoursesWithURLString:(NSString *)URLString;

@end

@protocol PurdueIOClientDelegate <NSObject>

@optional

- (void)purdueIOClient:(PurdueIOClient *)client didFetchTerms:(id)terms;
- (void)purdueIOClient:(PurdueIOClient *)client didFetchCourses:(id)courses;
- (void)purdueIOClient:(PurdueIOClient *)cleint didFetchSubjects:(id)subjects;
- (void)purdueIOClient:(PurdueIOClient *)client didFailWithError:(NSError *)error;

@end