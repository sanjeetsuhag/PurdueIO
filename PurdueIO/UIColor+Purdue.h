//
//  UIColor+Purdue.h
//  PurdueIO
//
//  Created by Sanjeet Suhag on 26/07/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Purdue)

+ (UIColor *)purdueYellowColor;

@end
