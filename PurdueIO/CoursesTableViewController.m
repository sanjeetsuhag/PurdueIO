//
//  CoursesTableViewController.m
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "CoursesTableViewController.h"
#import "AFNetworking.h"

#define kScreenWidth [[UIScreen mainScreen] applicationFrame].size.width
#define kScreenHeight [[UIScreen mainScreen] applicationFrame].size.height

@interface CoursesTableViewController ()

@property (strong, nonatomic) NSArray *courses;

// Properties for state restoration.
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation CoursesTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchResultsTableViewController = [[SearchResultsTableViewController alloc] init];
    self.searchResultsTableViewController.searchParentCode = 2;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsTableViewController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.searchBar.barStyle = UIBarStyleBlack;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchResultsTableViewController.tableView.delegate = self;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
    
    PurdueIOClient *client = [PurdueIOClient sharedPurdueIOClient];
    client.delegate = self;
    [client fetchCoursesWithURLString:[self courseURLStringForCourseString:self.selectedCourse]];
    [self.networkActivityIndicatorView startAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.searchControllerWasActive) {
        self.searchController.active = self.searchControllerWasActive;
        self.searchControllerWasActive = NO;
        
        if(self.searchControllerSearchFieldWasFirstResponder) {
            [self.searchController.searchBar becomeFirstResponder];
            self.searchControllerSearchFieldWasFirstResponder = NO;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - String Handling

- (NSString *)courseURLStringForCourseString:(NSString *)courseString {
    NSString *URLString = [NSString stringWithFormat:@"http://api.purdue.io/odata/Courses?$filter=Subject/Abbreviation eq '%@'&$orderby=Number asc", courseString];
    
    URLString = [URLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    URLString = [URLString stringByReplacingOccurrencesOfString:@"$" withString:@"%24"];
    URLString = [URLString stringByReplacingOccurrencesOfString:@"'" withString:@"%27"];
    
    return URLString;
}

#pragma mark - PurdueIOClient

- (void)purdueIOClient:(PurdueIOClient *)client didFetchCourses:(id)courses {
    self.courses = [courses objectForKey:@"value"];
    [self.tableView reloadData];
    [[self navigationItem] setRightBarButtonItem:nil];
}

- (void)purdueIOClient:(PurdueIOClient *)client didFailWithError:(NSError *)error {
    [[self navigationItem] setRightBarButtonItem:nil];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Fetching Data"
                                                                             message:@"Unable to connect to database."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              
                                                          }];
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];

}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.courses count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseCell" forIndexPath:indexPath];
    
    NSDictionary *temp = [self.courses objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [temp objectForKey:@"Title"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ credit(s)", [temp objectForKey:@"Number"], [temp objectForKey:@"CreditHours"]];
    
    // Set colors for UITableViewCell
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor purdueYellowColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UISearchBarUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchText = searchController.searchBar.text;
    
    NSMutableArray *searchResults = [self.courses mutableCopy];
    
    NSString *strippedSearchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSArray *searchItems = nil;
    if (strippedSearchText.length > 0) {
        searchItems = [strippedSearchText componentsSeparatedByString:@" "];
    }
    
    NSMutableArray *andMatchPredicates = [NSMutableArray array];
    
    for (NSString *searchString in searchItems) {
        
        // Full subject name matching.
        NSMutableArray *searchItemsPredicate = [NSMutableArray array];
        NSExpression *lhs = [NSExpression expressionForKeyPath:@"Title"];
        NSExpression *rhs = [NSExpression expressionForConstantValue:searchString];
        NSPredicate *finalPredicate = [NSComparisonPredicate
                                       predicateWithLeftExpression:lhs
                                       rightExpression:rhs
                                       modifier:NSDirectPredicateModifier
                                       type:NSContainsPredicateOperatorType
                                       options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        // Subject abbreviation matching.
        lhs = [NSExpression expressionForKeyPath:@"Number"];
        rhs = [NSExpression expressionForConstantValue:searchString];
        finalPredicate = [NSComparisonPredicate
                          predicateWithLeftExpression:lhs
                          rightExpression:rhs
                          modifier:NSDirectPredicateModifier
                          type:NSContainsPredicateOperatorType
                          options:NSCaseInsensitivePredicateOption];
        [searchItemsPredicate addObject:finalPredicate];
        
        NSCompoundPredicate *orMatchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:searchItemsPredicate];
        [andMatchPredicates addObject:orMatchPredicate];
    }
    
    NSCompoundPredicate *finalCompoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:andMatchPredicates];
    searchResults = [[searchResults filteredArrayUsingPredicate:finalCompoundPredicate] mutableCopy];

    SearchResultsTableViewController *tableController = (SearchResultsTableViewController *)self.searchController.searchResultsController;
    tableController.searchResults = searchResults;
    [tableController.tableView reloadData];
}

#pragma mark - UISearchControllerDelegate

- (void)presentSearchController:(UISearchController *)searchController {
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    // do something after the search controller is presented
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    // do something before the search controller is dismissed
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    // do something after the search controller is dismissed
}

@end
