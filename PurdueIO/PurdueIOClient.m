//
//  PurdueIOClient.m
//  PurdueIO
//
//  Created by Sanjeet Suhag on 25/07/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "PurdueIOClient.h"

static NSString * const kPurdueIOAPIBase = @"https://api.purdue.io/odata/";

static NSString * const kPurdueIOAPITerms = @"Terms";
static NSString * const kPurdueIOAPISubjects = @"Subjects";

@implementation PurdueIOClient

#pragma mark - Lifecycle

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    
    if(self) {
        self.requestSerializer  = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    return self;
}

+ (PurdueIOClient *)sharedPurdueIOClient {
    static PurdueIOClient *_sharedPurdueIOClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedPurdueIOClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kPurdueIOAPIBase]];
    });
    
    return _sharedPurdueIOClient;
}

- (void)fetchTerms {
    [self GET:kPurdueIOAPITerms
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          if([self.delegate respondsToSelector:@selector(purdueIOClient:didFetchTerms:)]) {
              [self.delegate purdueIOClient:self didFetchTerms:responseObject];
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          if([self.delegate respondsToSelector:@selector(purdueIOClient:didFailWithError:)]) {
              [self.delegate purdueIOClient:self didFailWithError:error];
          }
      }];
}

- (void)fetchCoursesWithURLString:(NSString *)URLString {
    [self GET:URLString
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          if([self.delegate respondsToSelector:@selector(purdueIOClient:didFetchCourses:)]) {
              [self.delegate purdueIOClient:self didFetchCourses:responseObject];
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          if([self.delegate respondsToSelector:@selector(purdueIOClient:didFailWithError:)]) {
              [self.delegate purdueIOClient:self didFailWithError:error];
          }
      }];
}

- (void)fetchSubjects {
    [self GET:kPurdueIOAPISubjects
   parameters:nil
     success:^(NSURLSessionDataTask *task, id responseObject) {
         if([self.delegate respondsToSelector:@selector(purdueIOClient:didFetchSubjects:)]) {
             [self.delegate purdueIOClient:self didFetchSubjects:responseObject];
         }
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         if([self.delegate respondsToSelector:@selector(purdueIOClient:didFailWithError:)]) {
             [self.delegate purdueIOClient:self didFailWithError:error];
         }
     }];
}

@end
