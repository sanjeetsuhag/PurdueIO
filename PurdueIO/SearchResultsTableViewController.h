//
//  SearchResultsTableViewController.h
//  PurdueIO
//
//  Created by Sanjeet Suhag on 26/07/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Purdue.h"

@interface SearchResultsTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *searchResults;
@property int searchParentCode;

@end
