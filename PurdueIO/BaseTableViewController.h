//
//  BaseTableViewController.h
//  PurdueIO
//
//  Created by Sanjeet Suhag on 10/08/15.
//  Copyright © 2015 vnev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController

@property (nonatomic, strong) UIActivityIndicatorView *networkActivityIndicatorView;

@end