//
//  ViewController.m
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray *termsArray;
@property (strong, nonatomic) NSArray *finishedTermArray;

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.networkActivityIndicatorView startAnimating];
    
    PurdueIOClient *client = [PurdueIOClient sharedPurdueIOClient];
    client.delegate = self;
    [client fetchTerms];
}

#pragma mark - PurdueIOClientDelegate

- (void)purdueIOClient:(PurdueIOClient *)client didFetchTerms:(id)terms {
    self.termsArray = [terms objectForKey:@"value"];
    [self.tableView reloadData];
    [[self navigationItem] setRightBarButtonItem:nil];
}

- (void)purdueIOClient:(PurdueIOClient *)client didFailWithError:(NSError *)error {
    [[self navigationItem] setRightBarButtonItem:nil];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Fetching Data"
                                                                             message:@"Unable to connect to database."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                          }];
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.termsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TermCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *tempDict = [self.termsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [tempDict objectForKey:@"Name"];
    
    // Set colors for UITableViewCell
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor purdueYellowColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];

    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}


@end
