//
//  ViewController.h
//  PurdueIO
//
//  Created by Vedant Nevetia on 7/24/15.
//  Copyright (c) 2015 vnev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PurdueIOClient.h"
#import "UIColor+Purdue.h"
#import "BaseTableViewController.h"

@interface ViewController : BaseTableViewController <PurdueIOClientDelegate>

@end